# Ponzu Demo

This is an implementation of the Ponzu Conference system.

For information on the features of the system, visit
[Ponzu](http://www.castle104.com/ponzu).

## Limitations

In order to provide Ponzu as open-source software, we 
separated the conference-specific code from the common code.

The common code is available from [Ponzu](https://bitbucket.org/castle104/ponzu)
as open source code under the BSD License. This is a Ruby gem (Rails Engine) 
and contains the bulk of the code.

The conference-specific code is available from [PonzuDemo](https://bitbucket.org/castle104/ponzudemo).
This is roughly the same code that is running on [Live Ponzu Demo](http://ponzu-demo.castle104.com).

Although [Ponzu](https://bitbucket.org/castle104/ponzu) is always fully up-to-date,
[PonzuDemo](https://bitbucket.org/castle104/ponzudemo) frequently lags behind.
This is because conference-specific code often contains information 
that is not owned by Castle104 nor the Molecular Biology Society of Japan (the copyright holders of Ponzu).
Therefore, we have to manually remove that information before updating [PonzuDemo](https://bitbucket.org/castle104/ponzudemo).

## Installation

Below are the steps for installation. These steps have been tested on a 
fresh install of Ubuntu Linux 13.10 Desktop.

### Get Ubuntu ready

1. Update packages.  
   `sudo apt-get update`

### Install MySQL

For installations other than Ubuntu, you may have to
set the password for the root user according to the
instructions in the MySQL documentation.

1. Install mysql  
   `sudo apt-get install mysql-server`  
   Ubuntu asks for a password for the root user.
   Set this to any password of your choice.  
   Also install developer headers.  
   `sudo apt-get install libmysqlclient-dev`  

### Install Ruby via RVM

We currently use Ruby 1.9.3 for development and deployment.
We use RVM to manage Ruby versions.

1. Install curl  
    `sudo apt-get install curl`
2. Install rvm  
   `\curl -sSL https://get.rvm.io | bash`  
   `source ~/.rvm/scripts/rvm`
2. Add the following to your `~.bashrc`  
   `source "$HOME/.rvm/scripts/rvm"`
4. Install Ruby 1.9.3 via rvm
   `rvm install ruby-1.9.3`
5. Make Ruby 1.9.3 the default  
   `rvm use 1.9.3 --default`
6. Check the version of Ruby  
   `ruby -v`  
   You should see the version of Ruby that you installed.

### Install Git

1. Install git.  
   `sudo apt-get install git`

### Install and set up PonzuDemo and Ponzu

Ponzu is loaded as a dependency by PonzuDemo.
We also provide a populated demo database which you
can download and use.

1. Clone PonzuDemo from the git repository  
   `git clone https://naofumi@bitbucket.org/castle104/ponzudemo.git`
2. Install dependencies  
   `cd ponzudemo`  
   `bundle install`
3. Setup database configuration    
   `cp config/database.yml.sample config/database.yml`  
   `nano config/database.yml`  
   Enter  
   `username: root`  
   `password: [the root pass for mysql]`  
   in the `development:` section.
4. Create the mysql database  
   `bundle exec rake db:create`  
   Provide the root password for mysql when prompted.
5. Download the demo database SQL file from [www.castle104.com](http://www.castle104.com/ponzu/open-source/).
6. Load the demo database.  
   Enter the Rails database console.  
   `bundle exec rails dbconsole`  
   From the console  
   `source /path/to/downloaded/sql/file`

### Start Solr

Ponzu uses Solr for search and also to find related presentations.
Some pages fail to load unless Solr is running. We use
the Jetty server included with the Sunspot gem for development.
Solr runs on Java, so we have to install that.

1. Install Java runtime
   `sudo apt-get install default-jdk`
2. Start the Solr server.
   `bundle exec rake sunspot:solr:start`
3. Reindex Solr
   `bundle exec rake sunspot:reindex_engines`

### Start Rails and see the Demo Page

1. Run the Rails server  
   `bundle exec rails s`
2. Access the site at http://localhost:3000

### Login as a user

Login with the user accounts set in the demo database. 
See [www.castle104.com](http://www.castle104.com/ponzu/open-source/) for account IDs and passwords.

### Rails Environment

The above instructions are for running Ponzu in development mode.
Castle104 generally runs Ponzu on Unicorn application servers
behind an NGINX web server. We also use memcached for caching.

The PonzuDemo repository has not been tested in production mode.
Please add your own configuration files and set up if this is what you
want to do.

## Futher documentation

We plan to provide further technical documentation on Ponzu at the
[Ponzu repository](https://bitbucket.org/castle104/ponzu).
