# This migration comes from ponzu_engine (originally 20131111041655)
class AddExternalLinkToPresentations < ActiveRecord::Migration
  def change
    add_column  :submissions, :external_link, :string
  end
end
