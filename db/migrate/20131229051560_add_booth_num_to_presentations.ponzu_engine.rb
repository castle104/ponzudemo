# This migration comes from ponzu_engine (originally 20131110104118)
class AddBoothNumToPresentations < ActiveRecord::Migration
  def change
    add_column  :presentations, :booth_num, :string
  end
end
