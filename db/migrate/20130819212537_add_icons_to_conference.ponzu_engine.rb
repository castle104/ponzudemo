# This migration comes from ponzu_engine (originally 20130819212351)
class AddIconsToConference < ActiveRecord::Migration
  def change
  	add_column	:conferences, :icons, :text
  end
end
