# This migration comes from ponzu_engine (originally 20131121073850)
class AddIndexToLikes < ActiveRecord::Migration
  def change
    add_index :likes, [:conference_tag, :presentation_id]
  end
end
