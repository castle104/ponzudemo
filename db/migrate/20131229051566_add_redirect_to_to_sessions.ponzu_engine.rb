# This migration comes from ponzu_engine (originally 20131119142436)
class AddRedirectToToSessions < ActiveRecord::Migration
  def change
    add_column  :sessions, :redirect_to, :string
  end
end
