# This migration comes from ponzu_engine (originally 20130824094122)
class AddKsCacheVersionToConference < ActiveRecord::Migration
  def change
  	add_column	:conferences, :ks_cache_version, :string
  end
end
