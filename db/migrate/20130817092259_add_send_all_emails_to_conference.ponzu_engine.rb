# This migration comes from ponzu_engine (originally 20130814144224)
class AddSendAllEmailsToConference < ActiveRecord::Migration
  def change
  	add_column	:conferences, :send_all_emails_to, :string, :default => nil
  end
end
