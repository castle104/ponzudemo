# This migration comes from ponzu_engine (originally 20131120052941)
class AddSecretToLikes < ActiveRecord::Migration
  def change
    add_column  :likes, :is_secret, :boolean, :default => false
  end
end
