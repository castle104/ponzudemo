# This migration comes from ponzu_engine (originally 20130819221446)
class AddConferenceHomePageUrlToConference < ActiveRecord::Migration
  def change
  	add_column	:conferences, :conference_home_page_url, :string
  end
end
