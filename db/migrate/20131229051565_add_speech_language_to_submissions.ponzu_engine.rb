# This migration comes from ponzu_engine (originally 20131115111055)
class AddSpeechLanguageToSubmissions < ActiveRecord::Migration
  def change
    add_column  :submissions, :speech_language, :string
  end
end
