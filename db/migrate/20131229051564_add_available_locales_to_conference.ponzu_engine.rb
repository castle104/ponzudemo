# This migration comes from ponzu_engine (originally 20131115003312)
class AddAvailableLocalesToConference < ActiveRecord::Migration
  def change
    add_column :conferences, :available_locales, :text
  end
end
