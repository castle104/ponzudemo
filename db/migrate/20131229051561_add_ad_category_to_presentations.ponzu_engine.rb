# This migration comes from ponzu_engine (originally 20131111033934)
class AddAdCategoryToPresentations < ActiveRecord::Migration
  def change
    add_column  :presentations, :ad_category, :string
    add_column  :sessions, :ad_category, :string
  end
end
