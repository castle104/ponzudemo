module ApplicationHelper
  # TODO: Monkey patch Array or Enumerable to
  #       add natural sort.
  def keys_for_natural_sort (string)
    string.split(/(\d+)/).map {|a| a =~ /\d+/ ? a.to_i : a }
  end
end
