# encoding: UTF-8

# Monkey patch SendPresentationAlerts in ponzu
# to add conference specific email tasks
module SendPresentationAlertsExtensions
  def self.included(base)
    base.extend ClassMethods
  end
  module ClassMethods
    def admin_users_to_notify
      []
      # @admin_users_to_notify ||= begin        
      #   admin = User.where(:conference_tag => 'ponzu_demo').find_by_login('admin')
      #   [admin]
      # end
    end
  end

end


