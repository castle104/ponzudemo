PonzuApp::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true



  ###############################
    # Test caching
  # config.action_controller.perform_caching = true
  # config.cache_store = :memory_store
  config.kamishibai_cache = false
  
  # ActionMailer configuration to use Gmail
  # http://guides.rubyonrails.org/action_mailer_basics.html#action-mailer-configuration-for-gmail
  config.action_mailer.delivery_method = :file # For testing
  # config.action_mailer.delivery_method = :smtp # For viewing the email in an email client
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.smtp_settings = {
    :address => "smtp.gmail.com",
    :port => 587,
    :domain => "castle104.com",
    :authentication => :plain,
    :enable_starttls_auto => true,
    :user_name => "demo-no-reply@castle104.com",
    :password => ""
  }
  config.action_mailer.default_url_options = {:host => "ponzu-demo.castle104.com"}
  
  # The email to which all messages will be sent to.
  # In production, set to false.
  # In development and testing, set to the email where you want to receive the response.
  # config.message_mailer_send_all_emails_to = "naofumi@mac.com"
  config.message_mailer_send_all_emails_to = false

  # Configure the protocol + host string that MessageMailer will use
  # Use Rails.message_mailer_bootstrap to read
  # http://stackoverflow.com/a/5053882/592808
  config.message_mailer_bootstrap = "http://development_server"

  # Don't use manifest usually in development
  config.use_manifest = false

end
